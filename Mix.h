#ifndef MIX_H
#define MIX_H

#include "Plant.h"

class Mix
{
  private:
  int waterPerc_;
  int herbicidePerc_;
  int fertilizerPerc_;

  public:

  Mix(int waterPerc, int herbicidePerc, int fertilizerPerc)
  {
    waterPerc_ = waterPerc;
    herbicidePerc_ = herbicidePerc;
    fertilizerPerc_ = fertilizerPerc;
  }

  void waterPlant(Plant &plant)
  {
    if (plant.isDead())
    {
      return;
    }

    if (plant.hydration(waterPerc_, herbicidePerc_) == 0)
    {
      return;
    }

    if (plant.killAttempt(herbicidePerc_) == 0)
    {
      return;
    }

    if (plant.growth(fertilizerPerc_) == 0)
    {
      return;
    }
  }

  void show()
  {
    std::cout << "The mix contains: \n";
    std::cout << "  " << waterPerc_ << "% of the Water\n";
    std::cout << "  " << herbicidePerc_ << "% of the Herbicide\n";
    std::cout << "  " << fertilizerPerc_ << "% of the Fertilizer\n";
  }
};

#endif
