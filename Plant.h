#ifndef PLANT_H
#define PLANT_H

#include <cstddef>
#include <iostream>

enum plantType
{
  error = -1,
  flower = 0,
  cactus,
  weed,

  plantTypeCount
};

class Plant
{
  private:

  int level_;
  plantType type_;

  public:

  Plant()
  {
    type_ = error;
    level_ = -1;
  }

  Plant(plantType type)
  {
    type_ = type;
    level_ = 1;
  }

  bool isDead()
  {
    return level_ == 0;
  }

  int getLevel()
  {
    return level_;
  }

  //
  // Does the plant die due to lack of the Water?
  //

  int hydration(int waterPerc, int herbicidePerc)
  {
    if (type_ == flower)
    {
      if (waterPerc < herbicidePerc)
      {
        level_ = 0;
      }
    }

    if (type_ == cactus)
    {
      if (waterPerc < 5)
      {
        level_ = 0;
      }
    }

    if (type_ == weed)
    {
      //
      // Nothing happens.
      //
    }

    return level_;
  }

  //
  // Does the plant die due to the effect of the Herbicide?
  //

  int killAttempt(int herbicidePerc)
  {
    int roll = (rand() % 100 + 1);

    if (type_ == flower)
    {
      if (roll < herbicidePerc / 2)
      {
        level_ = 0;
      }
    }

    if (type_ == cactus)
    {
      if (roll < herbicidePerc / 3)
      {
        level_ = 0;
      }
    }

    if (type_ == weed)
    {
      if (roll < herbicidePerc / level_)
      {
        level_ = 0;
      }
    }

    return level_;
  }

  //
  // Does the plant grow (gets +1 to its level)
  // due to the effect of the Fertilizer?.
  //

  int growth(int fertilizerPerc)
  {
    int roll = (rand() % 100 + 1);

    if (type_ == flower)
    {
      if (roll < fertilizerPerc)
      {
        level_++;
      }
    }

    if (type_ == cactus)
    {
      if (roll < fertilizerPerc / 2)
      {
        level_++;
      }
    }

    if (type_ == weed)
    {
      level_++;
    }

    return level_;
  }

  void show()
  {
    switch (type_)
    {
      case flower:
        std::cout << "F";
        break;
      case cactus:
        std::cout << "C";
        break;
      case weed:
        std::cout << "W";
        break;
      default:
        std::cout << "ERROR";
    }

    std::cout << level_;
  }
};


#endif
