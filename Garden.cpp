#include <cstddef>
#include "Mix.h"
#include "Plant.h"
#include <iostream>

//
// Additional, static parameters.
//

#define GARDEN_SIZE 5

#define BEFORE 0
#define AFTER  1

//
// Choose define/undef to enable/disable USER_VIEW.
//

#define USER_VIEW

void showGarden(Plant garden[][GARDEN_SIZE], int before);
void showSummary(double diePercentage[plantTypeCount],
                     double aliveAverageLevel[plantTypeCount]);

int main(int argc, const char **argv)
{
  if (argc != 5)
  {
    std::cout << "Not all required parameters are given.\n";
    std::cout << "Required are: <numberOfIterations> <waterPerc> <killerPerc> <fertPerc>.\n";

    return -1;
  }

  srand((unsigned int) time(NULL));

  //
  // Setting parameters from given argvs.
  //

  int maxIterations  = std::atoi(argv[1]);
  int waterPerc      = std::atoi(argv[2]);
  int herbicidePerc  = std::atoi(argv[3]);
  int fertilizerPerc = std::atoi(argv[4]);

  if (waterPerc + herbicidePerc + fertilizerPerc != 100)
  {
    std::cout << "ERROR! Given percentage do not sum to 100!\n";

    return -1;
  }

  Mix mix(waterPerc, herbicidePerc, fertilizerPerc);

  //
  // Create a garden with plants.
  //

  Plant garden[plantTypeCount][GARDEN_SIZE];

  for (int i = 0; i < GARDEN_SIZE; i++)
  {
    garden[flower][i] = Plant(flower);
    garden[cactus][i] = Plant(cactus);
    garden[weed][i] = Plant(weed);
  }

  #ifdef USER_VIEW

  showGarden(garden, BEFORE);

  std::cout << "\n";
  std::cout << "The following mix will be applied for "
            << maxIterations << " iterations.\n";

  mix.show();
  std::cout << "\n";

  #endif

  //
  // Add mix to every spot in the garden.
  //
  // Repeat maxIteration times.
  //

  for (int iter = 0; iter < maxIterations; iter++)
  {
    for (int i = 0; i < plantTypeCount; i++)
    {
      for (int j = 0; j < GARDEN_SIZE; j++)
      {
        Plant &plant = garden[i][j];

        mix.waterPlant(plant);
      }
    }
  }

  //
  // Check how many plants died.
  // Check what is the average level of the one who survived.
  //
  // Values split by plant type.
  //

  int dieCount[plantTypeCount];
  int aliveLevelCount[plantTypeCount];

  for (int i = 0; i < plantTypeCount; i++)
  {
    dieCount[i] = 0;
    aliveLevelCount[i] = 0;
  }

  for (int i = 0; i < plantTypeCount; i++)
  {
    for (int j = 0; j < GARDEN_SIZE; j++)
    {
      Plant &plant = garden[i][j];

      if (plant.isDead())
      {
        dieCount[i]++;
      }
      else
      {
        aliveLevelCount[i] += plant.getLevel();
      }
    }
  }

  double diePercentage[plantTypeCount];
  double aliveAverageLevel[plantTypeCount];

  for (int i = 0; i < plantTypeCount; i++)
  {
    diePercentage[i] = (double) dieCount[i] / GARDEN_SIZE;

    int aliveCount = GARDEN_SIZE - dieCount[i];

    if (aliveCount == 0)
    {
      aliveAverageLevel[i] = 0;
    }
    else
    {
      aliveAverageLevel[i] = (double) aliveLevelCount[i] / aliveCount;
    }
  }

  //
  // Show results.
  //

  #ifdef USER_VIEW

  showGarden(garden, AFTER);

  std::cout << "\n";

  showSummary(diePercentage, aliveAverageLevel);

  #else

  //
  // Show one-line summary that will be nice for Excel.
  //
  //

  std::cout << maxIterations << "\t"
            << waterPerc << "\t"
            << herbicidePerc << "\t"
            << fertilizerPerc << "\t";

  for (int i = 0; i < plantTypeCount; i++)
  {
    std::cout << diePercentage[i] << "\t"
              << aliveAverageLevel[i] << "\t";
  }

  std::cout << "\n";

  #endif
}

void showGarden(Plant garden[][GARDEN_SIZE], int before)
{
  std::cout << " Garden " << (before == 0 ? "before" : "after")
            << " simulation.\n";

  int length = GARDEN_SIZE * 6 - 1;

  //
  // Top border.
  //

  std::cout << "╔";

  for (int i = 0; i < length; i++)
  {
    std::cout << "═";
  }

  std::cout << "╗\n";

  //
  // Middle part.
  //

  for (int i = 0; i < plantTypeCount; i++)
  {
    std::cout << "║";

    for (int j = 0; j < GARDEN_SIZE; j++)
    {
      Plant &plant = garden[i][j];

      std::cout << " ";

      if (plant.isDead())
      {
        std::cout << "  ";
      }
      else
      {
        plant.show();
      }

      if (plant.getLevel() >= 10)
      {
        std::cout << " ";
      }
      else
      {
        std::cout << "  ";
      }

      if (j != GARDEN_SIZE - 1)
      {
        std::cout << "│";
      }
    }

    std::cout << "║\n";
  }

  //
  // Bottom border.
  //

  std::cout << "╚";

  for (int i = 0; i < length; i++)
  {
    std::cout << "═";
  }

  std::cout << "╝\n";
}

void showSummary(double diePercentage[plantTypeCount],
                     double aliveAverageLevel[plantTypeCount])
{
  for (int i = 0; i < plantTypeCount; i++)
  {
    int diePerc = diePercentage[i] * 100;

    switch (i)
    {
      case flower:
        std::cout << "Flower";
        break;
      case cactus:
        std::cout << "Cactus";
        break;
      case weed:
        std::cout << "Weed";
        break;
      default:
        std::cout << "ERROR";
        return;
    }

    std::cout << ":\n";
    std::cout << "  " << diePerc << "% died\n";
    std::cout << "  " << aliveAverageLevel[i]
              << " is the average level of survived ";

    switch (i)
    {
      case flower:
        std::cout << "flowers";
        break;
      case cactus:
        std::cout << "cactuses";
        break;
      case weed:
        std::cout << "weed";
        break;
      default:
        std::cout << "ERROR";
        return;
    }

    std::cout << ".\n";
  }
}
