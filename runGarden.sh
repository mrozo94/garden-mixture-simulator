#!/bin/bash

iterations=(10 10 10)
water=(     50 40 10)
killer=(    20 10 20)
fert=(      30 50 70)

numberOfSets="${#iterations[@]}"

for (( i = 0; i < $numberOfSets; i++ ))
do
  echo "Calling Garden with parameters ${iterations} ${water[i]} ${killer[i]} ${fert[i]}"
  echo "${i}/${numberOfSets}"

  ./Garden ${iterations} ${water[i]} ${killer[i]} ${fert[i]} >> log_script
done
