# This project was a part of MiniLecture that I gave for my students on 10.05.2023.

I have added few things and made code a bit clearer. It turned out quite good for such simple program, so I decided to put it as public.

The topic of MiniLecture was the optimization by scripts, why you should use it, and how to make your program become compatible with such.

Feel free to look through, change and compile your own versions.

## Description

There is a Garden with 3 rows of different plants.

- Flower [F]
- Cactus [C]
- Weed [W]

Plants are always placed in the same order: Flower, Cactus, Weed. Each fill its own row in the Garden.

We create a mixture of Water/Herbicide/Fertilizer to water every plant. The sum of those three must be equal to 100.
The goal is to check how different mixtures affects the Garden in the given time (number of iterations).

Watering plants consists of 3 steps.

1. **Does the plant die due to lack of the Water?** *(hydration)* It dies if:
    - Flower - the percentage of the Water is lower than the percentage of Herbicide.
    - Cactus - the percentage of the Water is lower than 5%.
    - Weed - never dies in this step.

1. **Does the plant die due to the effect of the Herbicide?** *(killAttempt)* It dies if a rolled 1-100 value:
    - Flower - is smaller than the half of the percengate of the Herbicide.
    - Cactus - is smaller than the third of the percengate of the Herbicide.
    - Weed - is smaller than the percentage of the Herbicide divided by the Weed level.

1. **Does the plant grow (gets +1 to its level) due to the effect of the Fertilizer?** *(growth)* It grows if a rolled 1-100 value:
    - Flower - is smaller than the percentage of the Fertilizer.
    - Cactus - is smaller than the half of the percentage of the Fertilizer.
    - Weed - is smaller than the percentage of the Fertilizer.

If any plant dies at any step it will be ignored until the end of the simulation.

At the end of the simulation, we calculate two statistics for every type of plant:
  - The percentage of dead plants of that type.
  - The average level of survived plants of that type.

## How to call?

    ./Garden.exe <numberOfIterations> <WaterPerc> <HerbicidePerc> <FertilizerPerc>

for example:

    ./Garden.exe 10 50 30 20

## Output for Excel

You can undef USER_VIEW to get one-line output usefull for Excel. Values are seperated by tabulator in the following order.

    // Parameters
    <numberOfIterations <WaterPerc> <HerbicidePerc> <FertilizerPerc>

    // Stats for flower, cactus, weed in that order. For each:
    <diePercentage> <aliveAverageLevel>

Files *log_userView* and *log_noUserView* contains the output for the example call with USER_VIEW defined and undefined respectively. Remember! Because of the usage of randomizer, different call can give different results, unless the same randomizer seed is given.

## Automatization

You can find a very basic example of a script that can be used to automatize any program, as long as its parameters are passed through the call parameters.

In the example, you can define any number of parameter sets. Output will be saved in the log file.

### Project picture

A picture of the flower came from [flaticon](https://www.flaticon.com/free-icon/flower_346218).
